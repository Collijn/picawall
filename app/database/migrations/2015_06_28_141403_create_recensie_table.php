<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecensieTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recensies', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('rating', 20);
            $table->boolean('goedgekeurd');
            $table->longText('omschrijving');
            $table->string('afbeelding', 100);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recensies');
	}

}
