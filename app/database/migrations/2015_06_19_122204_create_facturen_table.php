<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
    {
		Schema::create('facturen', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('afmeting_behang', 10);
            $table->string('afmeting_afbeelding', 10);
            $table->string('afbeelding', 255);
            $table->string('aanvullende_informatie', 500);
            $table->string('naam', 60);
            $table->string('email', 60);
            $table->string('straat_huisnummer', 60);
            $table->string('plaats', 60);
            $table->string('type', 15);
            $table->string('telefoon', 25);
            $table->string('postcode', 6);
            $table->string('behangen', 6);
            $table->string('prijs', 15);
            $table->dateTime('lever_datum');
            $table->timestamps();
		});


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('facturen');
	}

}
