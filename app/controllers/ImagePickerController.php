<?php

class ImagePickerController extends BaseController
{
    public function getIndex()
    {
        $images = array_diff(scandir(base_path().'/public/img/picker'), array('..', '.'));

        return View::make('layouts.imagePicker')->with('images', $images);
    }

    public function postUpload()
    {

        $factuur = New Factuur;
        if(Input::get('behang_width') AND Input::get('behang_height')){
            $factuur->afmeting_behang = Input::get('behang-width') . 'x' . Input::get('behang-height');
        }
        if(Input::get('width') AND Input::get('height')){
            $factuur->afmeting_afbeelding = Input::get('width') . 'x' . Input::get('height');
        }
        $factuur->afbeelding = Input::get('picker');
        $factuur->aanvullende_informatie = Input::get('aanvullende-informatie');
        $factuur->naam = Input::get('naam');
        $factuur->email = Input::get('email');
        $factuur->straat_huisnummer = Input::get('straat-huisnummer');
        $factuur->plaats = Input::get('plaats');
        $factuur->telefoon = Input::get('telefoon');
        $factuur->postcode = Input::get('postcode');
        $factuur->type = 'picker';
        $factuur->lever_datum = Input::get('lever-datum');
        $factuur->behangen = Input::get('behangen');
        $factuur->prijs = Input::get('prijs');
        $factuur->save();

        Mail::send('emails.factuur', array('factuur' => $factuur), function($message)
        {
            $message->to(Input::get('email'), Input::get('naam'))->subject('Bestelling Picawall');
        });

        Session::flash('message', 'U bestelling is verstuurd. Team Picawall zal u zo spoedig mogelijk contacteren');

        return Redirect::to('picker');

    }
}
