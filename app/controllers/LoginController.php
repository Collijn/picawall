<?php

class LoginController extends BaseController{
    public function getIndex()
    {
        return View::make('layouts.auth.login');
    }

    public function postLogin()
    {
        if(Input::get('username') == 'admin' AND Input::get('password') == 'admin')
        {
            $user = User::find(1);

            Auth::login($user);

            return Redirect::to('imageManager');
        }
    }
}