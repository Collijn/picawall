<?php

class ImageUploadController extends BaseController {


    public function getIndex()
    {
        return View::make('layouts.imageUpload');
    }

    public function postUpload()
    {
        if (Input::hasFile('sourceImage'))
        {
            $file = Input::file('sourceImage');
            $destinationPath = 'public/img/uploads/';
            $filename = $file->getClientOriginalName();
            Input::file('sourceImage')->move($destinationPath, $filename);

            $factuur = New Factuur;
            $factuur->afmeting_behang = Input::get('behang-width') . 'x' . Input::get('behang-height');
            $factuur->afmeting_afbeelding = Input::get('width') . 'x' . Input::get('height');
            $factuur->afbeelding = $filename;
            $factuur->aanvullende_informatie = Input::get('aanvullende-informatie');
            $factuur->naam = Input::get('naam');
            $factuur->email = Input::get('email');
            $factuur->straat_huisnummer = Input::get('straat-huisnummer');
            $factuur->plaats = Input::get('plaats');
            $factuur->telefoon = Input::get('telefoon');
            $factuur->postcode = Input::get('postcode');
            $factuur->lever_datum = Input::get('lever-datum');
            $factuur->behangen = Input::get('behangen');
            $factuur->prijs = Input::get('prijs');
            $factuur->type = 'upload';
            $factuur->save();


            $pdf = PDF::loadView('layouts.factuur', array('factuur' => $factuur))->setPaper('a4')->setWarnings(false)->save(base_path() . '/public/facturen/' . $factuur->id . '.pdf');

            Mail::queue('emails.factuur', array('factuur' => $factuur), function($message) use ($factuur)
            {
                $message->to(Input::get('email'), Input::get('naam'))->subject('Bestelling Picawall');
                $message->attach(base_path() . '/public/facturen/' . $factuur->id . '.pdf');
            });

            Session::flash('message', 'U bestelling is verstuurd. Team Picawall zal u zo spoedig mogelijk contacteren');

            return Redirect::to('upload');

        }
    }

}
