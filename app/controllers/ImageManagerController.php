<?php

class ImageManagerController extends BaseController{

  public function getManager()
  {

    $images = array_diff(scandir(base_path().'/public/img/picker'), array('..', '.'));

    return View::make('layouts.manager.imageManager')->with('images', $images);

  }

    public function getFacturenManager(){
        $facturen = Factuur::get();

        return View::make('layouts.manager.facturenManager')->with('facturen', $facturen);
    }

    public function getFacturenManagerShow($id)
    {
        $factuur = Factuur::where('id', '=', $id)->first();

        return View::make('layouts.manager.facturenManagerShow')->with('factuur', $factuur);
    }
  public function postDelete()
  {
    foreach(Input::get('images') as $image)
    {
      $imageName = str_replace('img/picker/', '', $image);

      unlink(base_path() . '/public/img/picker/' . $imageName);
    }
  }

  public function postUpload()
  {

    if (Input::hasFile('sourceImage'))
    {
        foreach(Input::file('sourceImage') as $image)
        {
          $file = $image;
          $destinationPath = 'public/img/picker/';
          $filename = $file->getClientOriginalName();
          $file->move($destinationPath, $filename);
        }
    }

      return Redirect::to('imageManager');
  }

}
