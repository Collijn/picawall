<?php

class RecensieController extends BaseController{
    public function getIndex(){
        $recensies = Recensie::where('goedgekeurd', '=', 1)->get();

        return View::make('layouts.recensie')->with('recensies', $recensies);
    }

    public function postSubmit()
    {
        $file = Input::file('sourceImage');
        $destinationPath = 'public/img/recensies/';
        $filename = $file->getClientOriginalName();
        Input::file('sourceImage')->move($destinationPath, $filename);

        $recensie = New Recensie;
        $recensie->rating = Input::get('rating');
        $recensie->omschrijving = Input::get('recensie');
        $recensie->afbeelding = $filename;
        $recensie->save();

        Session::flash('message', 'U recensie is verstuurd, De recensie moet nu goed gekeurd worden door een van onze medewerkers!');

        return Redirect::to('/recensie');
    }

    public function getManager()
    {
        $recensies = Recensie::get();

        return View::make('layouts.manager.recensieManager')->with('recensies', $recensies);
    }
}