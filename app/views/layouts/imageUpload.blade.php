@extends('layouts.master')

@section('content')
    @if(Session::has('message'))
        <div style="margin-top: 150px" class="fade modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div style="border: 4px dotted #758299" class="inner">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Bedankt voor uw bestelling</h4>
                        </div>
                        <div class="modal-body">
                            <p>Team Picawall neemt zo snel mogelijk contact met u op!</p>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    @endif
    <div class="container">
  <div class="row">
      <div class="col-md-7">

          <div id="foo">
              <ul class="slideme">
                  <li>
                      <label for="sourceImage">
                      <div class="mask1"></div>
                      </label>
                      <div class="source"></div>
                  </li>
                  <li>
                      <label for="sourceImage">
                          <div class="mask2"></div>
                      </label>
                      <div class="source"></div>
                  </li>
                  <li>
                      <label for="sourceImage">
                          <div class="mask3"></div>
                      </label>
                      <div class="source"></div>
                  </li>
              </ul>
          </div>
        </div>

    </div>

    <div class="info-col col-md-5">
      <h3>Upload jouw eigen foto!</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam posuere elit vel nulla congue, non pretium tellus facilisis.
            Curabitur sodales turpis felis, nec sodales massa convallis vel. Curabitur bibendum lorem vel justo tempor, ac porttitor mi
            aliquet. Maecenas at feugiat mauris. In vel auctor nunc. Aliquam erat volutpat. Nam a iaculis magna. Phasellus porta risus nec
            sapien luctus pellentesque.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam posuere elit vel nulla congue, non pretium tellus facilisis.
            Curabitur sodales turpis felis.</p>

        <p>Vereisten</p>

        <i class="fa fa-check"></i> 300 DPI <br />
        <i class="fa fa-check"></i> Lorum ipsum <br />
        <i class="fa fa-check"></i> adipiscing elit
    </div>

  </div>

  {{ Form::open( array('url' => 'upload', 'class' => 'uploadForm' , 'files'=> true) )}}

      <div class="row">

          <div class="col-md-3">

              <h3>Upload jouw foto</h3>
                <label for="sourceImage">
                  <img class="upload-computer" src="{{ asset('img/upload-computer.gif'); }}"/>
                </label>

                <input type="file" name="sourceImage" id="sourceImage">


          </div>

          <div class="col-md-4">
              <h3>Afmetingen afbeelding</h3>
              <input type="text" class="form-control input-width input-px" maxlength="3" name="width">CM breed
              <input type="text" class="form-control input-height input-px" maxlength="3" name="height">CM hoog
              <p style="color: #d5d5d5; margin-top: 7px;" class="small">Laat leeg voor volledige grootte!</p>
          </div>

          <div class="col-md-5">
              <h3>Afmetingen behang</h3>
              <input type="text" class="form-control input-behang-width input-behang-px" maxlength="4" name="behang-width">CM breed
              <input type="text" class="form-control input-behang-height input-behang-px" maxlength="4" name="behang-height">CM hoog
          </div>


      </div>

      <div class="row">
          <div class="col-md-10">
              <h3>Aanvullende informatie</h3>
              <TEXTAREA class="form-control input-informatie" Name="aanvullende-informatie" ROWS=7 COLS=140></TEXTAREA>
          </div>
      </div>

      <div class="row">
          <div class="col-md-9">
              <h3>Mijn gegevens</h3>
              <div class="form-group">
                  <input type="text" class="form-control input-gegevens" name="naam" placeholder="Naam">
                  <input type="text" class="form-control input-gegevens" name="email" placeholder="E-mail">
                  <input type="text" class="form-control input-gegevens" name="telefoon" placeholder="Telefoon">
              </div>
              <div class="form-group">
                  <input type="text" class="form-control input-gegevens" name="straat-huisnummer" placeholder="Straat + huisnummer">
                  <input type="text" class="form-control input-gegevens" name="plaats" placeholder="Plaats">
                  <input type="text" class="form-control input-gegevens" name="postcode" placeholder="Postcode">
              </div>

          </div>

          <div class="col-md-2">
              <div class="prijs">Prijs : €0,00</div>
              <input type="hidden" class="prijs-input" name="prijs">
              <input type="submit" value="Plaats bestelling" class="bestelling-submit" name="submitBtn">
          </div>
      </div>

        <div class="row">
            <div class="col-md-5">
                <input type="checkbox" class="behangenCheckbox" name="behangen" value="ja"> Wilt u het door onze behangers laten behangen?<br>
            </div>
        </div>
<br><br>
        <div class="row">
            <div class="col-md-6">

                <div style="display: none" class="agenda">
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' name="lever-datum" class="form-control" placeholder="Kies de gewenste leverdatum" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(function () {
                            $('#datetimepicker1').datetimepicker();
                        });
                    </script>
            </div>
        </div>

  {{ Form::close() }}
  </div>
  <script>
      var image;

      $(function () {
          $("#foo").slideme({
              arrows: true,
              autoslide : true,
              loop : true,
              interval : 5000,
          resizable: {
              width: 990,
              height: 450
          }
      });
      });

      $(function() {
          $(".behangenCheckbox").change(function() {
              if(this.checked) {
                  $(".agenda").show();
              } else {
                  $(".agenda").hide();
              }
          });
      });

      $(function() {

          $("#sourceImage").on("change", function()
          {
              var files = !!this.files ? this.files : [];
              if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

              if (/^image/.test( files[0].type)){ // only image file
                  var reader = new FileReader(); // instance of the FileReader
                  reader.readAsDataURL(files[0]); // read the local file

                  image = this.result;

                  reader.onloadend = function(){ // set image data as background of div
                      $( ".mask" ).css( "background-image", "url({{ asset('img/kamer2.gif'); }})" );
                      $( ".source" ).css( "background-image", "url("+this.result+")" );
                  }
              }
          });
      });

      $(function(){
          $(".input-px").on("change", function() {

            if ($(".input-width").val().length > 0 || $(".input-height").val().length > 0)
            {
                width = $(".input-width").val() + "cm";
                height = $(".input-height").val() + "cm";

                $(".source").css("background-size", width + ', ' + height);
            } else {
                $(".source").css("background-size", "100%" + "100%");
            }
          })
      })

      $(function(){
          $(".input-behang-px").on("change", function() {

              if ($(".input-behang-width").val().length > 0 || $(".input-behang-height").val().length > 0)
              {
                  width = $(".input-behang-width").val() / 100;
                  height = $(".input-behang-height").val() / 100;

                  oppervlakte = width * height;
                  kosten =  oppervlakte * 28 * 1.21;

                  $(".prijs").text('Prijs: €' + kosten.toFixed(2));
                  $(".prijs-input").val(kosten.toFixed(2));
              } else {
                  $(".prijs").text('€0,00');
              }
          })
      })
  </script>
@stop
