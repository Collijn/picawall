@extends('layouts.master')

@section('content')
    @if(Session::has('message'))
        <div style="margin-top: 150px" class="fade modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div style="border: 4px dotted #758299" class="inner">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">U recensie is verstuurd</h4>
                        </div>
                        <div class="modal-body">
                            <p>De recensie moet nu worden goed gekeurd door een van onze medewerkers!</p>
                        </div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    @endif
<div class="row">
    <div class="col-md-12">
        @foreach($recensies as $recensie)

            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img style="width: 400px !important; height: 280px !important;" class="media-object" src="{{ asset('img/recensies/'.$recensie->afbeelding); }}" >
                    </a>
                </div>
                <div class="media-body">
                    <fieldset class="rating">
                        <input type="radio" disabled  @if($recensie->rating == 5) checked="checked" @endif name="rating{{$recensie->id}}" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                        <input type="radio" disabled  @if($recensie->rating == '4 and a half') checked="checked" @endif name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                        <input type="radio" disabled  @if($recensie->rating == 4) checked="checked" @endif name="rating{{$recensie->id}}" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                        <input type="radio" disabled  @if($recensie->rating == '3 and a half') checked="checked" @endif name="rating{{$recensie->id}}" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                        <input type="radio" disabled  @if($recensie->rating == 3) checked="checked" @endif name="rating{{$recensie->id}}" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                        <input type="radio" disabled  @if($recensie->rating == '2 and a half') checked="checked" @endif name="rating{{$recensie->id}}" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                        <input type="radio" disabled  @if($recensie->rating == 2) checked="checked" @endif name="rating{{$recensie->id}}" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                        <input type="radio" disabled  @if($recensie->rating == '1 and a half') checked="checked" @endif name="rating{{$recensie->id}}" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                        <input type="radio" disabled  @if($recensie->rating == 1) checked="checked" @endif name="rating{{$recensie->id}}" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                        <input type="radio" disabled  @if($recensie->rating == 'half') checked="checked" @endif name="rating{{$recensie->id}}" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                    </fieldset>
                    <br><br><br>
                    <p>{{ $recensie->omschrijving }}</p>
                </div>
            </div>
        @endforeach
    </div>
</div>
<br><br><br>
    <div class="row">
        <div class="col-md-4">
            <label for="sourceImage">
                <div class="voorbeeld" style="width: 350px; cursor: pointer; height: 210px; margin-top: 110px; background-size: cover; background-image: url('{{ asset('img/placeholder.png'); }}');"></div>
            </label>
        </div>
        <div class="col-md-8">
            <h3>Vul je eigen recensie in!</h3>
            {{ Form::open( array('url' => '/submitRecensie', 'class' => 'uploadForm' , 'files'=> true) )}}
                <fieldset class="rating">
                    <input type="radio"  id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                    <input type="radio"  id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                    <input type="radio"  id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                    <input type="radio"  id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                    <input type="radio"  id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                    <input type="radio"  id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                    <input type="radio"  id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                    <input type="radio"  id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                    <input type="radio"  id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                    <input type="radio"  id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                </fieldset>
                <div class="form-group">
                    <TEXTAREA class="form-control input-informatie" placeholder="Recensie" name="recensie" ROWS=7 COLS=140></TEXTAREA>
                </div>
                <div class="form-group">
                    <input type="file" name="sourceImage" id="sourceImage">
                </div>
                <button type="submit" class="btn btn-default">Verstuur</button>
            </form>
        </div>
    </div>
<script>

    $(function() {

    $("#sourceImage").on("change", function()
    {
    var files = !!this.files ? this.files : [];
    if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

    if (/^image/.test( files[0].type)){ // only image file
    var reader = new FileReader(); // instance of the FileReader
    reader.readAsDataURL(files[0]); // read the local file

    image = this.result;

    reader.onloadend = function(){ // set image data as background of div
    $( ".voorbeeld" ).css( "background-image", "url("+this.result+")" );
    }
    }
    });
    });

</script>
@stop