<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>

    {{ HTML::style('css/style.css') }}
    {{ HTML::style('css/image-picker.css') }}
    {{ HTML::style('css/font-awesome.min.css') }}
    {{ HTML::style('css/picker-date.css') }}
    {{ HTML::style('css/slideme.css') }}

    {{ HTML::script('js/image-picker.min.js') }}
    {{ HTML::script('js/transition.js') }}
    {{ HTML::script('js/collapse.js') }}
    {{ HTML::script('js/bootstrap.js') }}
    {{ HTML::script('js/moment.js') }}
    {{ HTML::script('js/jquery.slides.js') }}
    {{ HTML::script('js/picker-date.js') }}
    {{ HTML::script('js/script.js') }}
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <title>Jumbotron Template for Bootstrap</title>
</head>

<body>

    <nav style="margin-bottom: 100px !important;" class="navigation" role="navigation">
        <ul class="navlist">
            <li><a href="/" id="home">Home</a></li>
            <li><a href="#" id="over">Over Picawall</a></li>
            <li><a href="#" id="afspraak">Maak Afspraak</a></li>
            <li><a href="/" id="logo"><img src="{{ asset('img/style/logo.png'); }}" width="100px" height="100px"></a></li>
            <li><a href="/recensie" id="recensies">Recensies</a></li>
            <li><a href="#" id="contact">Contact</a></li>
            <li><a href="#" id="social">Social Media</a></li>
        </ul>
    </nav>
    <div class="lijn"></div>




    <div class="container">

        @yield('content')
            <div class="row"><br><br>
                <div class="col-md-10 col-md-offset-1">
                    <p style="text-align: center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam posuere elit vel nulla congue, non pretium tellus facilisis. Curabitur sodales turpis felis, nec sodales massa convallis vel. Curabitur bibendum lorem vel justo tempor, ac porttitor mi aliquet. Maecenas at feugiat mauris. In vel auctor nunc. Aliquam erat volutpat. Nam a iaculis magna. Phasellus porta risus nec sapien luctus pellentesque.Lorem ipsum dolor sit amet, consectetur </p>
                    <p style="text-align: center">
                        <a href="#"><img style="width: 40px !important; height: 40px !important;" src="{{ asset('img/style/twitter.png') }}"></a>
                        <a href="#"><img style="width: 40px !important; height: 40px !important;" src="{{ asset('img/style/facebook.png') }}"></a>
                        <a href="#"><img style="width: 40px !important; height: 40px !important;" src="{{ asset('img/style/pint.png') }}"></a>
                    </p>
                </div>
            </div>
    </div>

</body>

<script>
    $(window).load(function () {
        $('#myModal').modal('show');
    });
</script>
</html>
