@extends('layouts.master')

@section('content')
    <div style="padding-top: 100px" class="row">
        <div class="col-md-12">
            <h3>Factuur #{{ $factuur->id }}</h3>
        </div>
    </div>
<br><br>
    <div class="row">
        <div class="col-md-5">
            <address>
                <strong>{{ $factuur->naam }}</strong><br>
                {{ $factuur->straat_huisnummer }}<br>
                {{ $factuur->plaats }}, {{ $factuur->postcode }}<br>
                {{ $factuur->telefoon }}<br>
                {{ $factuur->email }}
            </address>
        </div>
    </div>

    <div style="margin-bottom: 50px" class="row">
        <div class="col-md-5">
            <span style="margin-right: 20px"><strong>Factuurnummer:</strong></span>{{ $factuur->id }} <br>
            <span style="margin-right: 30px"><strong>Factuurdatum:</strong></span> {{ $factuur->created_at }}
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <table class="table">
                <tr>
                    <th>Omschrijving</th>
                    <th>prijs</th>
                </tr>
                <tr>
                    <td>
                        @if($factuur->afmeting_afbeelding)
                            <p><span style="margin-right: 20px">Afmeting afbeelding:</span> {{ $factuur->afmeting_afbeelding }}</p>
                        @endif
                        @if($factuur->afmeting_behang)
                            <p><span style="margin-right: 37px">Afmeting behang:</span> {{ $factuur->afmeting_behang }}</p>
                        @endif
                        @if($factuur->afbeelding)
                            <p><span style="margin-right: 78px">Afbeelding:</span> <a href="downloadAfbeelding/{{ $factuur->type }}/{{ $factuur->afbeelding }}">{{ $factuur->afbeelding }}</a></p>
                        @endif

                    </td>
                    <td>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>€{{ $factuur->prijs }}</p>
                    </td>
                </tr>
                <tr>
                    <td><span style="float: right;">21% BTW</span></td>
                    <td>€{{  round($factuur->prijs * 0.21, 2) }}</td>
                </tr>
                <tr>
                    <td><span style="float: right;">Totaal</span></td>
                    <td>€{{ round($factuur->prijs * 1.21, 2)  }}</td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <p><strong>Extra informatie</strong></p>
            {{ $factuur->aanvullende_informatie }}
        </div>
    </div>

    <br><br>

    <div class="row">
        <div class="col-md-6"><A HREF="javascript:window.print()">
                <IMG class="upload-computer no-print " SRC="{{ asset('img/print-btn.png'); }}"></A>
        </div>
    </div>
@stop