@extends('layouts.master')

@section('content')

  <div class="row">
      <div style="text-align: center; display: none;" class="alert alert-success" role="alert"></div>
      <div class="col-md-2">
          <ul class="nav nav-pills nav-stacked">
              <li role="presentation" class="active"><a href="imageManager">Image manager</a></li>
              <li role="presentation"><a href="facturenManager">Factuur manager</a></li>
              <li role="presentation"><a href="recensieManager">Recensie manager</a></li>
          </ul>
      </div>
      <div class="col-md-10">
          <select multiple="multiple" class="image-picker show-html">

              <option value=""></option>

              @foreach($images as $image)
                  <option data-img-src="img/picker/{{ $image }}" value="{{ $image }}"></option>
              @endforeach

          </select>

          <h3>Opties</h3>
          <form action="managerUpload" class="upload" method="post" enctype="multipart/form-data">

              <label for="sourceImage">
                  <img class="upload-computer" src="{{ asset('img/upload-manager.gif'); }}"/>
              </label>
              <img class="upload-computer delete"  src="{{ asset('img/delete-manager.gif'); }}"/>

            <input type="file" onchange="this.form.submit()" multiple class="btn file-upload btn-opties btn-success" name="sourceImage[]" id="sourceImage">
            <input style="display: none" type="submit" value="Upload Image" name="submitBtn">
          </form>
      </div>
  </div>


  <script>

      $("select").imagepicker()

      $(function(){
        $(".delete").on("click", function(){

          imgArray = [];

          $(".selected > img").each(function(){
              imgArray.push($(this).attr("src"));
          });


          $.ajax({
              url: 'managerDelete',
              data: { images: imgArray },
              type: "POST",
              success: function(response) {
                  console.log(response);
                $(".selected > img").each(function(){
                    $(this).parent().remove();
                });

                $('.alert-success').text('Afbeelding verwijderd!').fadeIn("slow").delay(5000).fadeOut('slow');
              }
          });

        })
      })


  </script>

@stop
