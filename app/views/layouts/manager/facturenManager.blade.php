@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-md-2">
            <ul class="nav nav-pills nav-stacked">
                <li role="presentation"><a href="imageManager">Image manager</a></li>
                <li role="presentation" class="active"><a href="facturenManager">Factuur manager</a></li>
                <li role="presentation"><a href="recensieManager">Recensie manager</a></li>
            </ul>
        </div>

        <div class="col-md-10">
            <table class="table">
                <tr>
                    <th>Factuur ID</th>
                    <th>Naam</th>
                    <th>email</th>
                    <th>Straat + huisnummer</th>
                    <th>Plaats + postcode</th>
                    <th>telefoon</th>
                    <th>Type</th>
                    <th>Prijs</th>
                </tr>
                @foreach($facturen as $factuur)
                    <tr>
                        <td><a href="facturenManager/{{ $factuur->id }}">#{{ $factuur->id }}</a></td>
                        <td>{{ $factuur->naam }}</td>
                        <td>{{ $factuur->email }}</td>
                        <td>{{ $factuur->straat_huisnummer }}</td>
                        <td>{{ $factuur->plaats }}, {{ $factuur->postcode }}</td>
                        <td>{{ $factuur->telefoon }}</td>
                        <td>{{ $factuur->type }}</td>
                        <td>€{{ round($factuur->prijs * 1.21, 2) }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@stop