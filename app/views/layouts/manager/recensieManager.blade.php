@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-md-2">
            <ul class="nav nav-pills nav-stacked">
                <li role="presentation"><a href="imageManager">Image manager</a></li>
                <li role="presentation"><a href="facturenManager">Factuur manager</a></li>
                <li role="presentation" class="active"><a href="recensieManager">Recensie manager</a></li>
            </ul>
        </div>

        <div class="col-md-10">
            <table class="table">
                <tr>
                    <th>Rating</th>
                    <th>Omschrijving</th>
                    <th>Afbeelding</th>
                    <th>Datum</th>
                    <th>goedgekeurd</th>
                </tr>
                @foreach($recensies as $recensie)
                    <tr>
                        <td>{{ $recensie->rating }}</td>
                        <td>{{ $recensie->omschrijving }}</td>
                        <td><img src="{{ asset('img/recensies/'.$recensie->afbeelding); }}"></td>
                        <td>{{ $recensie->created_at }}</td>
                        <td><input type="checkbox" @if($recensie->goedgekeurd == 1) checked="checked" @endif class="goedgekeurd" id="{{ $recensie->id }}" value="1"></td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

    <script>
        $(function() {
            $(".goedgekeurd").change(function() {
                if(this.checked) {
                    $.ajax({
                        url:"/recensiegoed",
                        method: 'post',
                        data: { val: $(this).val() , id : $(this).attr('id')},
                        success:function(data) {
                            return data;
                        }
                    });
                } else {
                    $.ajax({
                        url:"/recensieslecht",
                        method: 'post',
                        data: { val: $(this).val() , id : $(this).attr('id')},
                        success:function(data) {
                            return data;
                        }
                    });
                }
            });
        });
    </script>
@stop