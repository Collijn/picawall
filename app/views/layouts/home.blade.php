@extends('layouts.master')

@section('content')
<div style="padding-top: 120px" class="row">
    <div class="col-md-4">
        <a href="/upload"><img style="width: 100% !important; height: 100% !important;" src="{{ asset('img/style/blok1.png') }}"></a>
    </div>
    <div class="col-md-4">
        <a href="/picker"><img style="width: 100% !important; height: 100% !important;" src="{{ asset('img/style/blok2.png') }}"></a>
    </div>
    <div class="col-md-4">
        <a href="/ontwerpen"><img style="width: 100% !important; height: 100% !important;" src="{{ asset('img/style/blok3.png') }}"></a>
    </div>
</div>

@stop