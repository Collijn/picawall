<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('recensie', 'RecensieController@getIndex');
Route::post('submitRecensie', 'RecensieController@postSubmit');

Route::get('upload', 'ImageUploadController@getIndex');
Route::post('upload', 'ImageUploadController@postUpload');

Route::post('pickerUpload', 'ImagePickerController@postUpload');
Route::get('picker', 'ImagePickerController@getIndex');

Route::get('ontwerpen', 'ImageOntwerpenController@getIndex');
Route::post('ontwerpenUpload', 'ImageOntwerpenController@postUpload');

Route::get('login', 'LoginController@getIndex');
Route::post('login', 'LoginController@postLogin');

Route::get('/', function(){
    return View::make('layouts.home');
});

Route::post('recensiegoed', function(){
    $recensie = Recensie::find(Input::get('id'));
    $recensie->goedgekeurd = 1;
    $recensie->save();
});

Route::post('recensieslecht', function(){
    $recensie = Recensie::find(Input::get('id'));
    $recensie->goedgekeurd = null;
    $recensie->save();
});

Route::get('facturenManager/downloadAfbeelding/{type}/{image}', function($type, $image){
    if($type == 'picker')
    {
        return Response::download(public_path('img/picker/') . $image);
    } elseif($type == 'upload')
    {
        return Response::download(public_path('img/uploads/') . $image);
    }
});

Route::group(array('before' => 'auth|admin'), function ()
{
    Route::post('managerDelete', 'ImageManagerController@postDelete');
    Route::post('managerUpload', 'ImageManagerController@postUpload');

    Route::get('recensieManager', 'RecensieController@getManager');

    Route::get('manager', 'ManagerController@getIndex');
    Route::get('imageManager', 'ImageManagerController@getManager');
    Route::get('facturenManager', 'ImageManagerController@getFacturenManager');
    Route::get('facturenManager/{id}', 'ImageManagerController@getFacturenManagerShow');
});